<?php

/**
 * @category Bitbull
 * @package  Bitbull_DesignerCategory
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_DesignerCategory_Model_Observer
{

    /**
     * Adds custom handle to category page
     *
     * @param  Varien_Event_Observer $observer
     */
    public function controllerActionLayoutLoadBefore(Varien_Event_Observer $observer)
    {
        //if category is a child of category DESIGNERS
        if ($this->_isDesignerCategory() && !$this->_isDisplayingProducts()) {
            /* @var $update Mage_Core_Model_Layout_Update */
            $update = $observer->getEvent()->getLayout()->getUpdate();

            /**
             * add custom category handle
             */
            $update->addHandle('category_custom_designers');
        }
    }


    /**
     * Check if category pagination is active
     *
     * @return bool
     */
    protected function _isDisplayingProducts()
    {
        $requestParams = Mage::app()->getFrontController()->getRequest()->getParams();

        if (array_key_exists(
                Bitbull_DesignerCategory_Helper_Data::CATEGORY_QUERYSTRING_PAGINATION_PARAMETER_NAME, $requestParams
            )
        ) {
            return true;
        }

        return false;
    }


    /**
     * Check if category is child of Designer category
     *
     * @return bool
     */
    protected function _isDesignerCategory()
    {
        /** @var $category Mage_Catalog_Model_Category */
        $category = Mage::registry('current_category');

        /** @var $category Mage_Catalog_Model_Product */
        $product = Mage::registry('current_product');

        $designersCategoryId = $this->_getDesignersCategoryId();

        return (!$product
            && $category instanceof Mage_Catalog_Model_Category
            //&& $category->getLevel() == 3
            && $category->getParentId() == $designersCategoryId
        );
    }


    /**
     * Get designers category id from backend configuration
     *
     * @return int mixed
     */
    protected function _getDesignersCategoryId()
    {
        return Mage::getStoreConfig(Bitbull_DesignerCategory_Helper_Data::XML_PATH_DESIGNERS_CATEGORY_ID);
    }
}