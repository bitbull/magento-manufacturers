<?php

/**
 * @category Bitbull
 * @package  Bitbull_DesignerCategory
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_DesignerCategory_Block_Brand_Designers
    extends Mage_Core_Block_Template
{

    protected $_brandCategory;


    /**
     * Get designers category id
     *
     * @param $brandCategoryId
     *
     * @return Mage_Catalog_Model_Category
     */
    protected function _getBrandCategory($brandCategoryId)
    {
        if (is_null($this->_brandCategory)) {
            $this->_brandCategory = Mage::getModel("catalog/category")->load($brandCategoryId);
        }

        return $this->_brandCategory;
    }


    /**
     * Get designers cagtegory id
     *
     * @return string mixed
     */
    protected function _getBrandCategoryId()
    {
        return Mage::getStoreConfig(Bitbull_DesignerCategory_Helper_Data::XML_PATH_DESIGNERS_CATEGORY_ID);
    }


    /**
     * Get child categories for Brand
     *
     */
    protected function _getDesignersChildCategoriesData()
    {
        $designerCategoryId = $this->_getBrandCategoryId();
        $designerCategory = $this->_getBrandCategory($designerCategoryId);

        $designerCategoryCollection = $designerCategory->getCollection();
        $designerCategoryCollection->addAttributeToSelect('url_key')
            ->addAttributeToSelect('name')
            ->addAttributeToSelect('image')
            ->setOrder('name', Varien_Db_Select::SQL_ASC)
            ->addAttributeToFilter('is_active', 1)
            ->addIdFilter($designerCategory->getChildren())
            ->joinUrlRewrite();

        if($this->getFilter() && $this->getFilter() == "produttori_prodotti"){
            $catsProductIds = Mage::helper('bitbull_designercategory/designer')->getCategoryIdsFromLayerProducts();
            $designerCategoryCollection->addAttributeToFilter('entity_id', array('in' => $catsProductIds ));
        }
        $designersData = [];
        foreach ($designerCategoryCollection as $designerChildCategory) {
            $designersData[$designerChildCategory->getName()] = [
                "brand-name"        => $designerChildCategory->getName(),
                "brand-url-path"    =>
                    Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)
                    . $designerChildCategory->getRequestPath(),
                "brand-image-url"   => $designerChildCategory->getImageUrl(),
                "brand-category-id" => $designerChildCategory->getId()
            ];
        }
        return $designersData;
    }


    /**
     * Get designers data:
     * - designer logo
     * - designer name
     * - designer link
     */
    public function getDesignersData()
    {
        return $this->_getDesignersChildCategoriesData();
    }


    /**
     * Get current category ID
     */
    public function getCurrentCategoryId()
    {
        if (Mage::registry("current_category")) {
            return Mage::registry("current_category")->getId();
        }
        return false;
    }



}