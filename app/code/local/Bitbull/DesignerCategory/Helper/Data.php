<?php

/**
 * @category Bitbull
 * @package  Bitbull_DesignerCategory
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_DesignerCategory_Helper_Data
    extends Mage_Core_Helper_Abstract
{

    /**
     * Configuration path for designers category id
     */
    const XML_PATH_DESIGNERS_CATEGORY_ID = 'catalog/designers/category_id';

    /**
     * category pagination parameter name used on querystring
     */
    const CATEGORY_QUERYSTRING_PAGINATION_PARAMETER_NAME = "p";

    public Function getCategorieSorelle( Mage_Catalog_Model_Category $category){
        $sorelle = array();
        if($category->getParentId()){
            $parentCategory = Mage::getModel('catalog/category')->load($category->getParentId());
            foreach($parentCategory->getChildrenCategories() as $cat){
                if($cat->getId() != $category->getId()){
                    $sorelle[$cat->getId()] = $cat;
                }
            }
        }

        return $sorelle;

    }
}