<?php

/**
 * @category Bitbull
 * @package  Bitbull_DesignerCategory
 * @author   Dan Nistor <dan.nistor@bitbull.it>
 */
class Bitbull_DesignerCategory_Helper_Designer
    extends Mage_Core_Helper_Abstract
{

    /**
     * @var array container for all designers category ids
     */
    protected $_designersCategoryIds = array();

    /**
     * container for designer image url
     */
    protected $_designerImage = false;

    protected $_designerCategoryProduct = false;

    protected $_designerUrl = false;

    /**
     * Get designer category image
     *
     * @param $configurableProduct
     *
     * @return bool|string
     */
    public function getDesignerImage($product)
    {
        if($category = $this->_getDesignedProductCategory($product)){
            return $this->_getCategoryImageUrl($category->getId());
        }
        return false;

    }

    public function getDesignerName($product)
    {
        if($category = $this->_getDesignedProductCategory($product)){
            return $category->getName();
        }
        return false;

    }

    public function getDesignerDescription($product)
    {
        if($category = $this->_getDesignedProductCategory($product)){
            return $category->getDescription();
        }
        return false;

    }


    /**
     * Get category image
     *
     * @param $categoryId
     *
     * @return bool|string
     */
    protected function _getCategoryImageUrl($categoryId)
    {
        if ($this->_designerImage == false) {
            $category = Mage::getModel("catalog/category")->load($categoryId);
            $this->_designerImage = $category->getImageUrl();
        }
        return $this->_designerImage;
    }

    /**
      * Get category image
      *
      * @param $categoryId
      *
      * @return bool|string
      */
     protected function _getDesignedProductCategory($product)
     {
         if( !$this->_designerCategoryProduct[$product->getId()] ){
             $categoryIds = $product->getCategoryIds();
                      $designerCategoryIds = $this->_getAllDesignersCategoryIds();
                      if(is_array($categoryIds) && is_array($designerCategoryIds)){
                         $categoryIds = array_intersect($designerCategoryIds,$categoryIds);

                         if($categoryIds){
                             $categoryId = array_shift($categoryIds);
                             $this->_designerCategoryProduct[$product->getId()] = Mage::getModel("catalog/category")->load($categoryId);
                         }
                     }
         }


        return $this->_designerCategoryProduct[$product->getId()];
     }



    /**
     * Get all designers category ids, excluding cateogry Designer (parent)
     *
     * @return array|string
     */
    protected function _getAllDesignersCategoryIds()
    {
        if (empty($this->_designersCategoryIds)) {
            $designerCategory = Mage::getModel("catalog/category")->load($this->_getBrandCategoryId());
            $designersCategoryIds = $designerCategory->getAllChildren(true);
            // remove first element because is parent category;
            array_shift($designersCategoryIds);
            $this->_designersCategoryIds = $designersCategoryIds;
        }
        return $this->_designersCategoryIds;
    }


    /**
     * Get designers category id
     *
     * @return string mixed
     */
    protected function _getBrandCategoryId()
    {
        return Mage::getStoreConfig(Bitbull_DesignerCategory_Helper_Data::XML_PATH_DESIGNERS_CATEGORY_ID);
    }

    public function getDesignerUrl($configurableProduct)
    {

        $categoryIds = $configurableProduct->getCategoryIds();
        $designerCategoryIds = $this->_getAllDesignersCategoryIds();

        $categoryUrl = false;
        foreach ($categoryIds as $categoryId) {
            if (in_array($categoryId, $designerCategoryIds)) {
                $categoryUrl = $this->_getCategoryUrl($categoryId);
                break;
            }
        }

        return $categoryUrl;
    }

    protected function _getCategoryUrl($categoryId)
    {
        if ($this->_designerUrl == false) {
            $category = Mage::getModel("catalog/category")->load($categoryId);
            $this->_designerUrl = $category->getUrl();
        }
        return $this->_designerUrl;
    }


    public function getCategoryIdsFromLayerProducts(){
        $catIds = array();
        foreach(Mage::getSingleton('catalog/layer')->getProductCollection() as $product){
            if($product->getCategoryIds()){
                $designerProductCatIds = array_intersect($this->_getAllDesignersCategoryIds(),$product->getCategoryIds());
                $catIds = array_merge($catIds,$designerProductCatIds);
            }
        }
        return $catIds;
    }
}